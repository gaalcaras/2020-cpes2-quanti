% =================================================
%              L A T E X   P A P E R
%
%                by Gabriel Alcaras
% =================================================

% CHKTEX SETTINGS (see http://www.nongnu.org/chktex/ChkTeX.pdf)
% chktex-file 26 % enable spaces in front of punctuation (for French)

\documentclass[
  11pt, % Main document font size
  a4paper, % Paper type, use 'letterpaper' for US Letter paper
  oneside, % One page layout (no page indentation)
  headinclude,footinclude, % Extra spacing for the header and footer
  captions=figureheading, % Right references number in figure subfloats (https://tex.stackexchange.com/questions/98260/wrong-reference-number-for-subfloat)
  BCOR=5mm
]{scrartcl}

\input{../.setup/setup_paper.tex}

% =========================================
% HEADERS: title, author, affiliation, date
% =========================================

\renewcommand{\sectionmark}[1]{\markright{\spacedlowsmallcaps{Pratiques de la recherche (v. \gitcommithash)}}} % The header for all pages (oneside) or for even pages (twoside)

% The article title
\title{\normalfont\spacedlowsmallcaps{%
  Syllabus
  }\linebreak%
  Pratiques de la recherche
}

% The article author(s) - author affiliations need to be specified in the AUTHOR AFFILIATIONS block
\author{\spacedlowsmallcaps{Gabriel Alcaras*}}

% An optional date to appear under the author(s)
\date{CPES 2 · S4 · 2019--2020~\up{\dag}}

% Author affiliation
\newcommand{\authoraffiliation}{%
  \textit{Contacts : }%
  \texttt{\href{mailto:gabriel.alcaras@ehess.fr}{gabriel.alcaras@ehess.fr}}.
}

% ====================
% DOCUMENT STARTS HERE
% ====================

\begin{document}

% ==========
% FIRST PAGE
% ==========

% TABLE OF CONTENTS & LISTS OF FIGURES AND TABLES

\maketitle % Print the title/author/date block

% TOC
\setcounter{tocdepth}{2} % Set the depth of the table of contents to show sections and subsections only
\tableofcontents % Print the table of contents

% AUTHOR AFFILIATIONS
{\let\thefootnote\relax\footnotetext{\dag~\textit{Version du document : \gitcommithash.}}}
{\let\thefootnote\relax\footnotetext{* \authoraffiliation}}

% =========================
% WRITE YOUR PAPER HERE ;-)
% =========================

\section{Description et objectifs du cours}

Le cours a pour objectif d'initier les étudiant·e·s à la pratique de la
recherche en sciences sociales, en les accompagnant dans la réalisation
concrète d'une enquête et la production d'un petit mémoire de recherche. Ce
cours comporte trois aspects :

\begin{enumerate}
  \item Des séances magistrales présentant les principaux enjeux méthodologiques
    en sciences sociales ainsi que les «~ficelles du métier~».
  \item Une enquête que les étudiant·e·s mènent en binôme et en autonomie.
    Des séances seront régulièrement consacrées à faire le point de l'enquête
    en cours, et des travaux seront attendus pour rendre compte de l'avancement
    de chaque binôme. Néanmoins, l'essentiel du travail s'effectue en dehors du
    cours.
  \item Une initiation pratique aux statistiques par l'apprentissage d'un
    langage de programmation (R) et d'un logiciel compagnon (RStudio). Ces
    séances requièrent l'utilisation d'un ordinateur personnel.
\end{enumerate}

\section{Organisation du semestre}

Au deuxième semestre, le cours se dédouble :

\begin{itemize}
  \item Un Cours Magistral : en classe entière, tous les lundis de 10h15
    à 12h15, le cours portera spécifiquement sur des méthodes quantitatives
    plus avancées, dans la continuité du premier semestre.
  \item Des Travaux Dirigés : chaque groupe aura, un lundi sur deux de 8h
    à 10h, une séance de travaux dirigés destinée spécifiquement au suivi du
    travail de mémoire. Les TD sont encadrés par Timothée
    Erard\footnote{Contact:
    \href{mailto:timothee.erard@gmail.com}{timothee.erard@gmail.com}.}.
\end{itemize}

Voir le tableau récapitulatif des séances pour plus de
précisions~(p.~\pageref{tab:calendar}).

\section{Déroulement de l'enquête}

Au deuxième semestre, nous poursuivons l'enquête commencée au premier semestre.
Les principales étapes sont les suivantes :

\begin{itemize}
  \item Finalisation du questionnaire
  \item Diffusion et saisie du questionnaire
  \item Réalisation d'un second entretien
\end{itemize}

Voir le tableau récapitulatif des séances pour plus de précisions sur le
déroulé précis des différentes étapes~(p.~\pageref{tab:calendar}).

\section{Utilisation de R}

Au deuxième semestre, nous continuerons notre utilisation de R. \textbf{Il
faudra venir muni·e d'un ordinateur portable à chaque séance de cours
magistral}. Les paquets à installer : questionr, tidyverse, dplyr, ggplot2.

\section{Évaluation et validation}

Le cours se valide essentiellement sur le mémoire de recherche (manuscrit
rédigé, script R et compte-rendu d'entretien) et sa soutenance. Toutefois,
plusieurs rendus sont obligatoires tout au long du semestre.

\subsection{Mémoire de recherche}

Le mémoire doit présenter clairement une question de recherche, une
problématique et comment vos traitements statistiques permettent d’y répondre.
Vous devrez également mobiliser au moins un extrait d'entretien ou
d'observation dans votre mémoire.

\begin{itemize}
  \item \textbf{Le mémoire doit obligatoirement comporter un tableau et un
      graphique}.  Vous pouvez ensuite choisir d’y intégrer plus de tableaux ou
      plus de graphiques selon ce qui vous paraît le plus approprié. Nous
      attendons un \textbf{minimum de 3 traitements statistiques (tableaux et
      graphiques confondus)}. Toutefois, n’utilisez \textbf{pas plus de 6 ou
    7 traitements (graphiques et tableaux confondus) au total : privilégiez la
  qualité à la quantité}.
  \item Chaque partie doit comporter au moins un traitement statistique
    (tableau ou graphique) qui aide la démonstration.
  \item Le mémoire doit comporter au moins un extrait d'entretien ou
    d'observation.
  \item Le mémoire doit articuler les méthodes qualitatives et quantitatives.
  \item \textbf{Le mémoire doit être rendu accompagné du script R permettant de
    reproduire vos résultats, ainsi que de vos compte-rendus d'entretien.}
  \item \textbf{Le mémoire (hors annexe, bibliographie) doit faire entre 20 000
      et 25 000 signes, espaces comprises}. Cela fait une quinzaine de pages en
      interligne 1,5.  La limite de caractères est stricte : tout mémoire
      faisant moins de 20 000 signes ou plus de 25 000 signes se verra appliqué
      une pénalité d'un point.
\end{itemize}

\subsubsection{Critères d'évaluation}

Lisez attentivement les tableaux indiquant d'une part les critères d'évaluation
(p.~\pageref{tab:mem_eval}) et les pénalités en cas de non-respect des consignes
(p.~\pageref{tab:penalites}).

  \begin{table}[ht]
    \centering
    \caption{Critères d'évaluation}
    \label{tab:mem_eval}
    \begin{tabular}{llp{8cm}}
      \toprule
      Critère & Points & Détails \\
      \midrule
      Progression & 4 points & Progression sur le semestre : prise en
      considération des conseils et remarques.\\

      Statistiques & 3 points & Qualité du script R, pertinence des traitements
      effectués.\\

      Commentaires & 3 points & Commentaire du matériau mobilisé (tableau,
      graphe, entretien). Articulation quali / quanti.\\

      Entretiens & 2 points & Pertinence des entretiens ou des observations
      mobilisées.\\

      Présentation & 2 points & Mise en page du mémoire (plans, titres et
      sous-titres), des tableaux, des graphiques et des extraits d'entretien.\\

      Problématique & 2 points & Pertinence de la problématique, des questions
      de recherche et des réponses apportées.\\

      Bibliographie & 2 points & Qualité des sources, citations explicites et
      correctement formatées.\\

      Réflexivité & 2 points & Recul et retour sur le processus de recherche,
      d'enquête, de quantification, etc.\\
      \midrule
      Total & 20 points & \\
      \bottomrule
    \end{tabular}
  \end{table}

  \begin{table}[ht]
    \centering
    \caption{Points de pénalité}
    \label{tab:penalites}
    \begin{tabular}{llp{8cm}}
      \toprule
      Pénalité & Points & Détails \\
      \midrule
      Retard & -2 points & Le mémoire doit être envoyé le samedi 6 juin dernier
      délai.\\

      Format & -2 points & Les fichiers envoyés par email doivent être
      correctement formatés (mémoire au format pdf, entretiens et script
      R dans un fichier .zip).\\

      Nommage & -1 point & Les fichiers pdf (mémoire) et le .zip
      (contenant le script R et les entretiens) doivent être nommés sur le
      format :\break\ groupe\_nom1\_nom2.pdf.\\

      Consignes & -1 point & Le mémoire doit comporter au moins trois
      traitements statistiques (tableau, graphique, etc.), pas plus de 7. Il
      doit également comporter un extrait d'entretien ou d'observation.\\

      Longueur & -1 point & Le mémoire doit respecter la limite de signes
      (espaces comprises) indiquée.\\
      \bottomrule
    \end{tabular}
  \end{table}

\subsection{Soutenance de mémoire}

L’oral de soutenance se déroulera sur une durée de 30 minutes durant le lundi
08 juin (horaires de passage à déterminer) :

\begin{itemize}
  \item \textbf{15 minutes seront consacrées à la présentation de votre mémoire
    de recherche}.
    \begin{itemize}
      \item Le but de la présentation est de poser votre problématique et de
        montrer comment vous y avez répondu dans le mémoire à l'aide de
        traitements statistiques et d'entretiens
      \item Chaque membre du groupe devra prendre la parole et commenter un
        tableau ou un graphique du mémoire, en expliquant ce qu'il montre par
        rapport à votre question de recherche.
      \item Vous pouvez utiliser des diapositives comme support de
      présentation, mais ce n’est pas obligatoire.
  \end{itemize}
  \item \textbf{15 minutes de questions / réponses avec les membres du jury.}
\end{itemize}

\subsection{Travaux de suivi}

Un certain nombre de documents, à produire en groupe, doivent être rendus au
fur et à mesure du semestre. Ils ne font pas spécifiquement l'objet d'une
validation et ne peuvent pas affecter négativement la note si les consignes
sont respectées. \textbf{Ces rendus restent obligatoires pour valider le
semestre}.

\begin{itemize}
  \item Diffusion et saisie des questionnaires
  \item Une note d'étape
  \item Un compte-rendu d'entretien
  \item Un plan détaillé
\end{itemize}

Voir le tableau récapitulatif des séances pour plus de précisions sur le
déroulé précis des différentes étapes~(p.~\pageref{tab:calendar}).

\subsubsection{Diffusion et saisie des questionnaires}

Chaque groupe devra diffuser et saisir un certain nombre, défini à l'avance, de
questionnaires.

\subsubsection{Note d'étape}

La note d’étape (à rendre par voie électronique) doit présenter l'état de vos
réflexions et de vos travaux :

\begin{itemize}
  \item Formulez votre problématique et votre question de recherche.
  \item Donnez les références bibliographiques que vous comptez mobiliser.
  \item Donnez des idées de traitements statistiques (tableaux, graphiques) qui
    pourraient apporter des éléments de réponse à cette problématique.
  \item Listez les bases de données et les variables que vous souhaitez
    utiliser pour ces traitements, ainsi que les recodages / nettoyages
    nécessaires.
  \item Donnez quelques pistes à aborder lors d'un second entretien pour
    répondre à votre problématique.
  \item Vous pouvez également donner les problèmes rencontrés et anticipés pour
    la suite de l'analyse.
\end{itemize}

Ce document doit faire au maximum 3 pages en interligne 1,5. Ce n’est pas une
dissertation, vous pouvez donc utiliser des listes à puces, des intertitres,
etc.

\subsubsection{Un compte-rendu d'entretien}

Au deuxième semestre, vous avez plus de latitude pour réaliser votre entretien.
Il doit essentiellement vous permettre d'apporter des éléments de réponse
à votre problématique. Vous pouvez préparer une grille d'entretien, mais ce
n'est pas obligatoire. Vous pouvez aussi choisir de faire un entretien long ou
court ; directif, libre ou semi-directif.

Le compte-rendu d'entretien est également moins formel qu'au premier semestre.
Il doit présenter :

\begin{itemize}
  \item Un rappel de votre problématique
  \item Un bref paragraphe de mise en contexte ethnographique de l'entretien
  \item Une retranscription, au moins partielle, des moments de l'entretien qui
    vous semblent pertinents pour votre objet de recherche.
\end{itemize}

Notez que l'entretien peut avoir été réalisé pendant la passation du
questionnaire.

\subsubsection{Plan détaillé}

Le plan détaillé doit donner la structure à venir de votre mémoire de
recherche. Regardez également la section consacrée au mémoire de recherche pour
avoir une idée de ce qui est attendu dans chaque partie.

Le plan détaillé doit :
\begin{itemize}
  \item Formuler clairement votre problématique et votre question de recherche.
  \item Donner les titres des parties et des sous-parties.
  \item Présenter au moins un tableau et un graphique.
  \item Pour les traitements qui n’ont pas encore été réalisés, vous pouvez
    simplement dire ce que vous comptez faire.
\end{itemize}

\section{Séances de TD}

\begin{table}[ht]
  \centering
  \caption{Séances de TD}%
  \label{tab:td}
  \begin{tabular}{@{}lllll@{}}
    \toprule
    Date & G & Séance & Cours & À rendre pour la séance\\
    \midrule
    03 fev. & 1 & \multirow{2}{*}{$\Big\}\text{~1}$} & \multirow{2}{*}{Objet,
       bibliographie} & \\

    10 fev. & 2 & & & \\

    24 fev. & 1 & \multirow{2}{*}{$\Big\}\text{~2}$}
       & \multirow{2}{*}{Suivi passation du questionnaire} \\

    02 mar & 2 & & \\

    09 mar. & 1 & \multirow{2}{*}{$\Big\}\text{~3}$}
       & \multirow{2}{*}{Présentation de la note d'étape} \\

    16 mar. & 2 & & \\

    23 mar. & 1 & \multirow{2}{*}{$\Big\}\text{~4}$} & \multirow{2}{*}{Travaux
       sur R} & \multirow{2}{*}{Note d'étape}\\

    30 mar. & 2 & & \\

    20 avr. & 1 & \multirow{2}{*}{$\Big\}\text{~5}$} & \multirow{2}{*}{Travaux
       sur R} \\

    27 avr. & 2 & & \\

    04 mai & 1 & \multirow{2}{*}{$\Big\}\text{~6}$} & \multirow{2}{*}{Travaux
       sur R} & \multirow{2}{*}{Compte-rendu d'entretien}\\

    11 mai & 2 & & \\

    18 mai & 1 & \multirow{2}{*}{$\Big\}\text{~7}$}
       & \multirow{2}{*}{Rédaction} & \multirow{2}{*}{Plan détaillé}\\

    25 mai & 2 & & \\
    \bottomrule

  \end{tabular}
\end{table}

\section{Calendrier des séances}

Voir tableau récapitulatif~\ref{tab:calendar}, p.~\pageref{tab:calendar}.

\begin{landscape}
  \begin{table}
    \centering
    \begin{tabular}{@{}lllllrl@{}}
    \toprule
    \#                                               & Date    & Cours Magistral           & Enquête                  & Rendus CM             & TD & Rendus TD                                      \\
    \midrule
    01                                               & 03 fev. & Masque de saisie          &                          &                       & 1  &                                                \\
    02                                               & 10 fev. & Recodage et nettoyage     & Diffusion questionnaire  &                       & 2  &                                                \\ [0.6em]
    \multicolumn{7}{c}{\rule[3pt]{8cm}{0.2pt}~\textit{Vacances d'hiver}~\rule[3pt]{8cm}{0.2pt}}   \\ [0.5em]
    03                                               & 24 fev. & Recodage et nettoyage     &                          &                       & 1  &                                                \\
    04                                               & 02 mar  & Recodage et nettoyage     &                          &                       & 2  &                                                \\
    06                                               & 09 mar. & Statistique inférentielle &                          &                       & 1  &                                                \\
    05                                               & 16 mar. & Statistique inférentielle &                          & Questionnaires saisis & 2  &                                                \\
    07                                               & 23 mar. & Statistique inférentielle & Deuxième entretien       &                       & 1  & \multirow{2}{*}{$\Big\}\text{~Note d'étape}$}  \\
    08                                               & 30 mar. & Statistique inférentielle &                          &                       & 2  &                                                \\ [0.5em]
    \multicolumn{7}{c}{\rule[3pt]{8cm}{0.2pt}~\textit{Vacances de Pâques}~\rule[3pt]{8cm}{0.2pt}}   \\ [0.5em]
    09                                               & 20 avr. & Visualisation de données  &                          &                       & 1  &                                                \\
    10                                               & 27 avr. & Visualisation de données  &                          &                       & 2  &                                                \\
    11                                               & 04 mai  & Analyse textuelle         &                          &                       & 1  & \multirow{2}{*}{$\Big\}\text{~CR entretien}$}  \\
    12                                               & 11 mai  & Analyse textuelle         & Traitement des résultats &                       & 2  &                                                \\
    13                                               & 18 mai  & Analyse textuelle         &                          &                       & 1  & \multirow{2}{*}{$\Big\}\text{~Plan détaillé}$} \\
    14                                               & 25 mai  & Analyse textuelle         &                          &                       & 2  &                                                \\ [0.5em]
    \multicolumn{7}{c}{\rule[3pt]{8cm}{0.2pt}~\textit{Lundi de Pentecôte}~\rule[3pt]{8cm}{0.2pt}}   \\ [0.5em]
    15                                               & 08 jui. & Soutenances des mémoires  &                          & Mémoires le 6 juin    &    &                                                \\
     \bottomrule
     \smallskip%
    \end{tabular}

    \smallskip%

    \caption{Calendrier synthétique des séances}\label{tab:calendar}
  \end{table}
\end{landscape}
\end{document}
